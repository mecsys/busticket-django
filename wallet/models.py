from django.db import models

# Create your models here.

class Cost(models.Model):
    
    cost = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return '{}'.format(self.cost)

class Carriage(models.Model):
    name = models.CharField(max_length=200)
    pub_date = models.DateTimeField('data published')
    cost = models.ForeignKey(Cost, on_delete=models.CASCADE)    

    def __str__(self):
        return self.name

class Ticket(models.Model):
    CARRIAGE_WAY = (
        ('I', 'Ida'),
        ('V', 'Volta'),        
    )
    carriage = models.ForeignKey(Carriage, on_delete=models.CASCADE)
    way = models.CharField(max_length=1, choices=CARRIAGE_WAY, default='I')
    date = models.DateTimeField('date published')

    def __str__(self):
        return '{} {}'.format(self.date, self.carriage)

class Balance(models.Model):
    balance = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return '{}'.format(self.balance)

class Wallet(models.Model):
    cpf = models.CharField(max_length=11, primary_key=True)
    nome = models.CharField(max_length=200)    
    balance = models.ForeignKey(Balance, on_delete=models.CASCADE)

    def __str__(self):
        return self.nome
