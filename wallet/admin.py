from django.contrib import admin

# Register your models here.

from django.contrib import admin

from .models import Carriage, Cost, Wallet, Balance

admin.site.register(Carriage)
admin.site.register(Cost)
admin.site.register(Wallet)
admin.site.register(Balance)
