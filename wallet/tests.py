import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

# Create your tests here.

from .models import Balance

class BalanceModelTests(TestCase):

    def test_add_balance(self):
        """
        test add balance.        
        """
        balance = 100.00
        future_question = Balance(balance = balance)
        self.assertEqual(future_question.balance, 100.00)
